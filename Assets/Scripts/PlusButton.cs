﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlusButton : MonoBehaviour
{
    [SerializeField] GameObject bigDescMenu, bigDescText;

    private void OnMouseDown()
    {
        if (!bigDescMenu.activeSelf)
        {
            bigDescMenu.SetActive(true);
            bigDescText.SetActive(true);
        }
    }
}
