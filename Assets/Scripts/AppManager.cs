﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManager : MonoBehaviour
{
    [SerializeField] GameObject[] BigDescElementsToUnactive;
    private void Update()
    {
        if (SceneManager.GetActiveScene().name == "AREnglish")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("StartMenuEnglish");
            }
        }
        else if (SceneManager.GetActiveScene().name == "ARFrench")
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                SceneManager.LoadScene("StartMenu");
            }
        }
    }
    public void CloseBigDesc()
    {
        for (int i = 0; i < BigDescElementsToUnactive.Length; i++)
        {
            BigDescElementsToUnactive[i].SetActive(false);
        }
    }
}
