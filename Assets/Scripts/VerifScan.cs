﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VerifScan : MonoBehaviour
{
    public Button verif;

    public Image verifImage;
    // Start is called before the first frame update
    void Start()
    {
        verifImage = verif.image;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ChangeColor()
    {
        verifImage.color = Color.green;
        
    }
}
