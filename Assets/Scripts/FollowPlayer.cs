﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [HideInInspector] public bool isInConfiguration;
    void Update()
    {
        transform.position = Camera.main.transform.position;
        if (isInConfiguration)
        {
            transform.rotation = Quaternion.Euler(0, Camera.main.transform.rotation.eulerAngles.y, 0);
        }
    }
    public void SetIsInConfiguration(bool isInConfiguration)
    {
        this.isInConfiguration = isInConfiguration;
    }
}
