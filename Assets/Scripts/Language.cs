﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Language : MonoBehaviour
{
    public void English()
    {
        if (SceneManager.GetActiveScene().name != "StartMenuEnglish")
        {
            SceneManager.LoadScene("StartMenuEnglish");
        }
    }

    public void French()
    {
        if (SceneManager.GetActiveScene().name != "StartMenu")
        {
            SceneManager.LoadScene("StartMenu");
        }
    }

    public void StartEnglish()
    {
        SceneManager.LoadScene("AREnglish");
    }

    public void StartFrench()
    {
        SceneManager.LoadScene("ARFrench");
    }

    public void CreditFrench()
    {
        SceneManager.LoadScene("Crédit");
    }

    public void CreditEnglish()
    {
        SceneManager.LoadScene("CréditEnglish");
    }
}
