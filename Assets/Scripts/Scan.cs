﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scan : MonoBehaviour
{
    public GameObject verify;

    public GameObject scanImage;

    void Update()
    {
        if (verify.activeSelf)
        {
            scanImage.SetActive(false);
        }
        else
        {
            scanImage.SetActive(true);
        }
    }
}
