﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    [SerializeField] GameObject bigDescMenu;
    GameObject lilDescription;
    bool showsLilDesc;
    private void Start()
    {
        lilDescription = transform.GetChild(0).gameObject;
    }
    private void OnMouseDown()
    {
        if (!bigDescMenu.activeSelf)
        {
            showsLilDesc = !showsLilDesc;
            lilDescription.SetActive(showsLilDesc);
        }
    }
}
