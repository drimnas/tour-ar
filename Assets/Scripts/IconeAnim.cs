﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IconeAnim : MonoBehaviour
{
    float timer;
    bool start;
    Vector3 originalScale, zoomedScale;
    [SerializeField] GameObject spriteGM, lilDescription;
    [SerializeField] float zoom = 3f;
    private void Start()
    {
        originalScale = transform.localScale;
        zoomedScale = originalScale * zoom;
    }
    void Update()
    {
        //transform.LookAt(Camera.main.transform.position, Vector3.forward); 
        //transform.Rotate(new Vector3(0, 180, 0));
        //fonctionne sur unity mais pas dans le build android

        if (timer >= 1) start = false;
        if (start)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0;
        }
        //spriteGM.transform.localRotation = Quaternion.Euler(0, 0, Mathf.Lerp(0, 360, timer));
        if (lilDescription.activeSelf && start)
        {
            transform.localScale = Vector3.Lerp(originalScale, zoomedScale, timer);
        }
        else if (start)
        {
            transform.localScale = Vector3.Lerp(zoomedScale, originalScale, timer);
        }
    }

    private void OnMouseDown()
    {
        start = true;
    }
}
